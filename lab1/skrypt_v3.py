import random
import numpy as np
from matplotlib import pyplot as plt
import matplotlib.animation as animation
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.path import Path
from matplotlib.patches import PathPatch
from math import *

delta = 0.04
x_size = 10
y_size = 10
#obst_vect = [(-4, 5), (-2, -5), (5, 2), (3, -3)]
obst_vect = [(random.randint(-10, 10),random.randint(-10, 10)), (random.randint(-10, 10),random.randint(-10, 10)), (random.randint(-10, 10),random.randint(-10, 10)),(random.randint(-10, 10),random.randint(-10, 10))]
# start_point = (-10, -3)
# finish_point = (10, 3)
start_point=(-10,random.randint(-10, 10))
finish_point=(10,random.randint(-10, 10))

x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = X + Y

D0 = 3.5 # Ogranicza maksymalny promień odpychania przeszkody
DR = D0
Kp = 0.02 # Zmienia siłę przyciągającą do celu (im większa wartość, tym większa siła [mniejszy promień]).
Ko = 10 # Zmienia siłę odpychającą od przeszkody (im większa wartość, tym większa siła [większy promień])

# Funckja licząca odległość pomiedzy punktami
def dist(p1, p2):
    return np.sqrt(dist2(p1, p2))


def norm(v):
    l = len(v)
    v = (v[0]/l, v[1]/l)
    return v

def len(v):
    return np.sqrt(v[0]**2 + v[1]**2)


# funkcja licząca wektor z podanych punktów
def vec2(p1, p2):
    v = (p2[0]-p1[0], p2[1]-p1[1])
    return v

# Funckja licząca kwadrat odległość pomiedzy punktami
def dist2(p1, p2):
    dx = p1[0] - p2[0]
    dy = p1[1] - p2[1]
    return dx**2 + dy**2

# Funkcja opisująca potencjał przyciągający
def up(qr, qk):
    d2 = dist2(qr, qk)
    return 0.5*Kp*d2


# Funkcja licząca siłę przyciągająca
def fp(qr, qk):
    return Kp*dist(qr, qk)



# Funkcja licząca  siłę odpychającą
def fo(qr, qo):
    d = dist(qr, qo)
    if d > D0:
        return 0
    else:
        v = Ko*(1/d - 1/D0)*(1/(d**2))
        return v

# Funkcja opisująca potencjał odpychajacy
def vo(qr, qo):
    d = dist(qr, qo)
    if d > D0:
        return 0
    else:
        return 0.5*Ko*(1/d - 1/D0)**2

# Funkcja wyliczająca sumę wszystkich sił odpychających
def fo_vec(qr, obst_v):
    f = 0
    for obst in obst_v:
        f += fo(qr, obst)
    return f

# Funkcja wyliczając wartość siły dla podanego punktu qr
def map_p(qr, qk, obst_v):
    return fo_vec(qr, obst_v) + fp(qr, qk)




def vec_sum(v1, v2):
    v = (v1[0]+v2[0], v1[1]+v2[1])
    return v

def vec_mul(v, f):
    vr = (v[0]*f, v[1]*f)
    return vr

for i, iv in enumerate(X):
    for j, jv in enumerate(Y):
        px = iv[i]
        py = jv[j]
        Z[j][i] = map_p((px, py), finish_point, obst_vect)

path = []
def Path_Finder():
    p = start_point
    i = 400
    while i > 0:
        i = i-1

        k = finish_point

        # wektor przyciagajacy
        vp_f = fp(p,k)
        vp = vec2(p, k)
        vp = norm(vp)
        vp = vec_mul(vp, vp_f)
        # wektor odpychajacy - suma
        vo_s = (0,0)

        # sily odpychajace
        for obst in obst_vect:
            vo_f = fo(p, obst)
            vo = vec2(p, obst)

            if len(vo) > DR:
                vo_f = 0

            vo = norm(vo)
            vo = vec_mul(vo, vo_f)
            vo_s = vec_sum(vo_s, vo)

        v_dir = norm(vec_sum(vp, vec_mul(vo_s, -1)))

        dx = round(v_dir[0])
        dy = round(v_dir[1])

        p = (p[0]+dx/10, p[1]+dy/10)
        path.append(p)


fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjalow')
# ax.xlabel("X")
# ax.ylabel("Y")
line, = ax.plot([], [], lw=2) 
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size], vmax=1, vmin=-1)

plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

# initialization function 
def init(): 
    # creating an empty plot/frame 
    line.set_data([], []) 
    return line, 

# lists to store x and y axis points 
xdata, ydata = [], [] 

# animation function 
def animate(i): 
    try:
    # t is a parameter 
        t = path[i] 
    
        # x, y values to be plotted 
        x = t[0] 
        y = t[1] 
    
        # appending new points to x, y axes points list 
        xdata.append(x) 
        ydata.append(y) 
        line.set_data(xdata, ydata) 
        return line,
    except IndexError:
        return line,

Path_Finder()  

# call the animator  
anim = animation.FuncAnimation(fig, animate, init_func=init, frames=10000, interval=20, blit=True) 


plt.colorbar(orientation='vertical')
# for p in path:
    # plt.plot(p[0], p[1], "or", color='red', markersize=1)

plt.grid(True)
plt.show()
