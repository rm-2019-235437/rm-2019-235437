import random
import array
import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.path import Path
from matplotlib.patches import PathPatch


kp=1
d0=20
koi = [1,1,1,1]

delta = 0.01
x_size = 10
y_size = 10
obst_vect = [(random.randint(-10, 10),random.randint(-10, 10)), (random.randint(-10, 10),random.randint(-10, 10)), (random.randint(-10, 10),random.randint(-10, 10)),(random.randint(-10, 10),random.randint(-10, 10))]

#X_stop = #print("Randomowa liczba: ",X_rand)

start_point=(9,random.randint(-10, 10))
finish_point=(-9,random.randint(-10, 10))


x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = np.exp(-X**0)
Voi = np.empty((len(obst_vect)))

print("Długość obst:",len(obst_vect))
print("dlugość Voi:",len(Voi))

'''
for i in range(len(obst_vect)):
    if np.any(np.subtract(start_point, finish_point)) <= d0:
        Voi[i] = koi[i]/2 * (1/(np.subtract(start_point, finish_point)) - (1/d0))**2
    else:
        Voi[i] = 0
    print(Voi[i])
''' 
print(type(Z))
print(Z)

Fp = np.zeros((2000,2000))
print(Fp)
print(type(Fp))


Up = (kp/2)*abs(np.subtract(start_point, finish_point))**2
Uw = Up + np.sum(obst_vect)


fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjałów')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size],
           vmax=1, vmin=-1)


plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

plt.colorbar(orientation='vertical')

plt.grid(True)
plt.show()
