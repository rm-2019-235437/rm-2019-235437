import random
import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.path import Path
from matplotlib.patches import PathPatch
 
delta = 0.1
x_size = 10
y_size = 10

ko = 2 # Zmienia siłę odpychającą od przeszkody (im większa wartość, tym większa siła [większy promień])
d0 = 40 # Ogranicza maksymalny promień odpychania przeszkody
kp = 0.017 # Zmienia siłę przyciągającą do celu (im większa wartość, tym większa siła [mniejszy promień]).

# Funckja licząca kwadrat odległość pomiedzy punktami
def dist2(p1, p2):
    dx = p1[0] - p2[0]
    dy = p1[1] - p2[1]
    return dx**2 + dy**2
    
#Liczenie odległości między punktami zdefiniowanymi tuplami
def qDist(q1,q2):
     return np.sqrt((q1[0] - q2[0])**2 + (q1[1] - q2[1])**2)
    
#Liczenie siły przyciągającej
def Fp(qr,qk,kp):
    return kp*qDist(qr,qk)
    
#Liczenie sił odpychających od przeszkody
# ko -> koi
def Fo(qr,qo,ko,d0):
    if qDist(qr,qo)>d0:
        return 0
    if qDist(qr,qo) == 0:
        return 10
    return -ko*(1/qDist(qr,qo)-1/d0)*(1/(qDist(qr,qo)**2))

#Liczenie sił odpychających tablicy przeszkód dla punktu
def TFo(qr, tp, ko, d0):
    TFp=0
    for p in tp:
        TFp += (Fo(qr,p,ko,d0))
    return TFp

def Kierunek(Z, start, step):
    try:
        # if Z[start[0]][start[1]] >= Z[start[0]][start[1]+step]:
        print("Jeden",type(Z))
        p = Z[int((start[0]+step)/delta+1/delta*10)][int(start[1]/delta+1/delta*10)]

        print("Dwa",type(p))
        # elif Z[start[0]][start[1]] >= Z[start[0]+step][start[1]+step]:
        gp = Z[(start[0]+step) / delta+1/delta*10][(start[1]+step) / delta+1/delta*10]
        # elif Z[start[0]][start[1]] >= Z[start[0]+step][start[1]]:
        g = Z[start[0]/delta+1/delta*10][(start[1]+step) / delta+1/delta*10]
        # elif Z[start[0]][start[1]] >= Z[start[0]+step][start[1]-step]:
        lg = Z[(start[0]-step) / delta+1/delta*10][(start[1]+step) / delta+1/delta*10]
        # elif Z[start[0]][start[1]] >= Z[start[0]][start[1]-step]:
        l = Z[(start[0]-step) / delta+1/delta*10][start[1] / delta+1/delta*10]
        # elif Z[start[0]][start[1]] >= Z[start[0]-step][start[1]-step]:
        ld = Z[(start[0]-step) / delta+1/delta*10][(start[1]-step) / delta+1/delta*10]
        # elif Z[start[0]][start[1]] >= Z[start[0]-step][start[1]]:
        d = Z[start[0] / delta+1/delta*10][(start[1]-step) / delta+1/delta*10]
        # elif Z[start[0]][start[1]] >= Z[start[0]-step][start[1]+step]:
        pd = Z[(start[0]+step) / delta+1/delta*10][(start[1]-step) / delta+1/delta*10]
        li = [p, gp, g, lg, l, ld, d, pd]
        minimum = min(li)
        if minimum == p:
            print("prawo")
            return (start[0]+step,start[1])
        elif minimum == gp:
            print("górne prawo")
            return (start[0]+step,start[1]+step)
        elif minimum == g:
            print("góra")
            return (start[0],start[1]+step)
        elif minimum == lg:
            print("lewa góra")
            return (start[0]-step,start[1]+step)
        elif minimum == l:
            print("lewo")
            return (start[0]-step,start[1])
        elif minimum == ld:
            print("lewy dół")
            return (start[0]-step,start[1]-step)
        elif minimum == d:
            print("dół")
            return (start[0],start[1]-step)
        elif minimum == pd:
            print("prawy dół")
            return (start[0]+step, start[1]-step)
    except IndexError:
        pass

def Krok(point, finish_point, Z):
    moves = list()
    moves.append(point)
    while point!=finish_point:
        moves.append(Kierunek(Z, point, 1))
        point = moves[len(moves)-1]
        if len(moves) == 20:
            break

    for obstacle in moves:
        plt.plot(obstacle[0], obstacle[1], "or", color='green')
    return moves



obst_vect = [(random.randint(-10, 10),random.randint(-10, 10)), 
            (random.randint(-10, 10),random.randint(-10, 10)), 
            (random.randint(-10, 10),random.randint(-10, 10)),
            (random.randint(-10, 10),random.randint(-10, 10))]


start_point=(-9,random.randint(-10, 10))
finish_point=(9,random.randint(-10, 10))



print("Start: " + str(start_point))
print("Finish: " + str(finish_point))



x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = np.exp(-X**0)

n=0

for punktY in y:
    for punktX in x:
        n+=1
        Zy=int(punktY/delta+1/delta*10)
        Zx=int(punktX/delta+1/delta*10)
        Z[Zy,Zx] = TFo((punktX,punktY), obst_vect,ko, d0) + Fp((punktX,punktY), finish_point, kp)

# [0][0] = (-10,-10)
# [200][200] = (10,10)

# print("TEST ",int(-10.0/delta+1/delta*10))
# print("TEST2 ",int(10.0/delta+1/delta*10))


fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjałów')

plt.imshow(Z, cmap=cm.RdYlGn,
            origin='lower', extent=[-x_size, x_size, -y_size, y_size],
            vmax=1, vmin=-1)


plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

# print(Z[start_point[0]][start_point[1]])
# print(Z[start_point[0]][start_point[1+1]])1

# print(Kierunek(Z, start_point, finish_point, 1))

Krok(start_point,finish_point, Z)

plt.colorbar(orientation='vertical')

plt.grid(True)
plt.show()





